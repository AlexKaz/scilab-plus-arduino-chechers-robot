// Copyright (C) Alexander Kazantsev (AlexKaz, alexkazancev@bk.ru)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


// This is a Arduino C++ code

// v013 05/04/2019

// If this code is usuful for you donate please, https://yoomoney.ru/to/410019721796762

// mega2560
// управление только со Scilab через передачу строк
// три двигателя
// полушаговый режим работы для шаговых двигателей (8 отчётов)

//пины двигателя захвата
const int pinHvat1 = A7;
const int pinHvat2 = A6;

// Драйверы A4988

const int impPoper = 470;
const int impProd = 230;
const int impVert = 3350;
const int impPoper_8 =470/2;
const int impProd_8 = 230/2;
const int impVert_8 = 3350/2;

const int delay2 = 100;

// кнопка пульта
const int buttonPin = A1;     // the number of the pushbutton pin
int buttonState;

// обозначения движков
const int mProd = 2;
const int mPoper = 1;
const int mVert = 3;

// движок продол подачи:
//выводы 4 и 5 двигателя никуда не подключены
const int in11 = 10; //вывод 1 двигателя
const int in12 = 11; //вывод 2 двигателя
const int in13 = 12; //вывод 5 двигателя
//const int in14 = 8; //вывод 6 двигателя
const int regim1 = 1;
const int dl1 = 5000;//5000;//1400;


// движок попереч подачи
// шаговый EM-142 от матричного принтера
// управляется без драйвера, напрямую с пинов
//Передо мной однажды встал такой вопрос, вот как я его решил:
//двигатель шаговый был от какого то старого матричного принтера EM-142 даташита я на него не нашел. Из разъема просто торчит 6 штырей, никаких цветовых обозначений нет. После изучения технической литературы по шаговикам, сделал вывод, что двигатель униполярный - две обмотки с отводами из середины, так оно и оказалось. Прозванивался он в моем случае так: выводы делятся на 2 группы, в каждой они прозваниваются между собой. в каждой группе есть два вывода, между которыми сопротивление было 26 ом - это выводы катушек, они через драйвер двигателя (я использовал К1128КТ3А ) подключаются к ардуине. Оставшийся вывод имеет сопротивление по отношению к остальным двум - 12-13 ом. Его надо подключить к питанию "силовой" части, я использовал 12 вольт. Итого: из 6 выводов 2 подключаются к питанию, в моем случае это был средние выводы: 3 и 4.
//1, 2, 5, 6 выводы подключаются к драйверу.
//Сперва после подключения двигатель вел себя странно: дергался, но не крутился.
//Поменял в скетче местами управляющие выводы - все заработало, стало быть я при
//подключении двигателя неверно подключил обмотки. Двигатель после некоторого времени
//прямо таки раскалился - скорее всего следовало начинать с 5 вольт напряжения.
const int in21 = 5;//2;
const int in22 = 2;//3;
const int in23 = 12;//4;
//const int in24 = 5;
const int dl2 = 10000; //2500; // очень быстро двигается при 2500
const int regim2 = 1;


// движок вертикальной подачи
// шаговый 28BYJ-48
// управляется с пом. своего драйвера 16TYDDZM ULN2003APG
const int in31 = 3; //вывод 1 двигателя
const int in32 = 4; //вывод 2 двигателя
const int in33 = 12; //вывод 5 двигателя
//const int in34 = 13; //вывод 6 двигателя
const int dl3 = 1900;// 700;//1000; //50000; //константа двигателя
const int regim3 = 1;
//#define DEBUG

void setup() {
  pinMode(in11, OUTPUT);
  pinMode(in12, OUTPUT);
  pinMode(in13, OUTPUT);
//  pinMode(in14, OUTPUT);

  pinMode(in21, OUTPUT);
  pinMode(in22, OUTPUT);
  pinMode(in23, OUTPUT);
//  pinMode(in24, OUTPUT);

  pinMode(in31, OUTPUT);
  pinMode(in32, OUTPUT);
  pinMode(in33, OUTPUT);
//  pinMode(in34, OUTPUT);

  pinMode(pinHvat1,OUTPUT);
  pinMode(pinHvat2,OUTPUT);


  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);

  //  pinMode(pinHIGH,OUTPUT);
  //  analogWrite(pinHIGH,HIGH);


  Serial.begin(9600);
}

int i0=1;
int j0=1;
int k0=1;

int chislo;

#define led 13
String input_string = "";
char tmpstr[10];

boolean regim = false; // режим false - настройка поля (текущие координаты игнорируются), true - координаты настроены.

void loop() {
 label:

  mydigitalWrite( in13, LOW );
//  mydigitalWrite( in23, LOW );
//  mydigitalWrite( in33, LOW );

  while (Serial.available() > 0) {
     delay(100);//
      char c = Serial.read();
      if (c == '\n') { 
        Serial.print("Input_string is: ");
        Serial.println(input_string);
        strcpy(tmpstr,input_string.c_str()); //преобразовать String в массив char
        if (tmpstr[1]=='m'){
          chislo =-( tmpstr[2] - '0');}
        else {
          chislo =+( tmpstr[1] - '0');
        }
        Serial.println(chislo);
        if (tmpstr[0]=='X') {
           Serial.println("Prodolnaya podacha");
           if (!regim) {mymove2(chislo,0,0);}
           else {mymove(chislo,j0,k0);}
        }
        else if (tmpstr[0]=='Y') {
         Serial.println('Poperechnaya podacha');
           if (!regim) {mymove2(0,chislo,0);}
           else {mymove(i0,chislo,k0);}
        }
        else if (tmpstr[0]=='Z'){
          Serial.println('Vertikalnaya podacha');
           if (!regim) {mymove2(0,0,chislo);}
           else {mymove(i0,j0,chislo);}
        }
        else if (tmpstr[0]=='H'){ // хват
          Serial.println("Zahvat");
          if (tmpstr[1]!='m'){
            chislo=chislo*100+(( tmpstr[2] - '0')*10)+ (tmpstr[3] - '0');
          }
          else{
            chislo=chislo*100-(( tmpstr[3] - '0')*10)- (tmpstr[4] - '0');
          }
          
          zahvat(chislo);
        }
        else if (tmpstr[0]=='R'){
          Serial.println("Regim");
          if (tmpstr[1]=='1'){
            regim=true;
            i0=tmpstr[2]-'0'; j0=tmpstr[3]-'0'; k0=tmpstr[4]-'0'; 
          }
          else{
            regim=false;
          }
        }
        input_string = "";
        } else {
        input_string += c;
      }
    }



  goto label;
}

bool hvat=false;

void zahvat(int chislo){
  Serial.println(chislo);
  //if (hvat){
    for (int i=1; i<=100; i++){
      if (chislo<0){
        analogWrite(pinHvat1,1023);
        analogWrite(pinHvat2,0);
        delay(abs(chislo));
        analogWrite(pinHvat1,0);
      }
      else {
        analogWrite(pinHvat2,1023);
        analogWrite(pinHvat1,0);
        delay(abs(chislo));
        analogWrite(pinHvat2,0);
      }
      
    }

  
}

int mymove3(int ii,int ii0,int impDvig_8,int  mNapr,int  pin1,int pin2,int pin3,int dl,int reverse){

 int napravlenie = 0;
  
 if (ii!=ii0) {
    if ((ii>ii0) & (ii<9)){
       napravlenie=1;
       if (reverse!=0) {napravlenie=2;}
    }
    else if ((ii<ii0) & (ii>=1)){
       napravlenie=2;
       if (reverse!=0) {napravlenie=1;}
    }
 }
  if (napravlenie==1) {
    for (int tt=abs(ii-ii0);tt>=1; tt--) {
       // Serial.print ("forward ");  Serial.println(tt);
        for (int iii=1; iii<=impDvig_8; iii++){
         forward2  (mNapr, pin1, pin2, pin3, 0, dl, regim2);  delayMicroseconds(delay2);  }
        if (reverse) {ii0=ii0-1;}
        else {ii0=ii0+1;}
    }
  }
  if (napravlenie==2){
    for (int tt=abs(ii0-ii); tt>=1; tt--) {
      //Serial.print("backward "); Serial.println(tt);
      for (int iii=1; iii<=impDvig_8; iii++){
        backward2(mNapr, pin1, pin2, pin3, 0, dl, regim2); delayMicroseconds(delay2); }
        if (reverse) {ii0=ii0+1;}
        else {ii0=ii0-1;}
       }
    }
    
 return ii0;
}

  


void mymove (int i, int j, int k){
     // Serial.print("i0 j0 k0 "); Serial.print(" "); Serial.print(i0); Serial.print(" "); Serial.print(j0); Serial.print(" "); Serial.println(k0);
    // Serial.print("mymove "); Serial.print(" "); Serial.print(i); Serial.print(" "); Serial.print(j); Serial.print(" "); Serial.println(k);
    // двигаемся на конкретную позицию из известной позиции
    i0=mymove3(i,i0,impProd_8,mProd,in21,in22,in23,dl2,1);  //продольно:вперед-назад1
    j0=mymove3(j,j0,impPoper_8,mPoper,in11,in12,in13,dl1,0);  // двигаемся на 1 клетку поперек:в одну сторону-в другую сторону
    k0=mymove3(k,k0,impVert_8,mVert,in31,in32,in33,dl3,0);  // двигаемся на 1 клетку поперек:вниз-вверх

   // Serial.print("i0 j0 k0 "); Serial.print(" "); Serial.print(i0); Serial.print(" "); Serial.print(j0); Serial.print(" "); Serial.println(k0);
}

void mymove2 (int i, int j, int k){
  //просто двигаемся на один+ шаг двигателя

  if (i>0) {
  // двигаемся  продольно:вперед
   for (int ii=1; ii<=abs(impProd_8/4); ii++){
     backward2  (mProd, in21, in22, in23, 0, dl2, regim2);     delayMicroseconds(delay2);
   }}
  if (i<0){
  // двигаемся  продольно:назад
    for (int ii=1; ii<=abs(impProd_8/4); ii++){
      forward2(mProd, in21, in22, in23,0, dl2, regim2);     delayMicroseconds(delay2);
  }}
  if (j>0) {
   // двигаемся поперек:в одну сторону
    for (int ii=1; ii<=abs(impPoper_8/4); ii++){
      forward2(mPoper, in11, in12, in13, 0, dl1, regim1);         delayMicroseconds(delay2);
    }}
  if (j<0) {
    // двигаемся поперек:в другую сторону
     for (int ii=1; ii<=abs(impPoper_8/4); ii++){
      backward2(mPoper, in11, in12, in13, 0, dl1, regim1);        delayMicroseconds(delay2);
    }}
  
  if (k>0) {
   // двигаемся  вниз
    for (int ii=1; ii<=abs(impVert_8/4); ii++){
      forward2(mVert, in31, in32, in33, 0, dl3, regim3);        delayMicroseconds(delay2);
    }  }
  if (k<0) {
    // двигаемся на 1 клетку вверх
    for (int ii=1; ii<=abs(impVert_8/4); ii++){
      backward2(mVert, in31, in32, in33, 0, dl3, regim3);        delayMicroseconds(delay2);
    }}
  
}

void mydigitalWrite(int intpin, int level) {
  if (level == HIGH) {
    if ((intpin >= 10) & (intpin<=13)) {
      PORTB |= (B00000001 << (intpin - 6));
    }
    if ((intpin >= 6) & (intpin<=9)){
      PORTH |= (B00000001 << (intpin - 3));
    }

    if (intpin == 2) {
      //DDRD |=(B00000001<<(intpin-14));
      PORTE |= (B00000001 << (intpin + 2));
    }
    if (intpin == 3) {
      //DDRD |=(B00000001<<(intpin-14));
      PORTE |= (B00000001 << (intpin + 2));
    }

    if (intpin == 4) {
      //DDRD |=(B00000001<<(intpin-14));
      PORTG |= (B00000001 << (intpin + 1));
    }
    if (intpin == 5) {
      //DDRD |=(B00000001<<(intpin-14));
      PORTE |= (B00000001 << (intpin - 2));
    }
  }
  else {
    if ((intpin >= 10) & (intpin<=13) ){
      PORTB &= ~(B00000001 << (intpin - 6));
    }
    if ((intpin >= 6) & (intpin<=9)){
      PORTH &= ~(B00000001 << (intpin - 3));
    }

    if (intpin == 2) {
      //DDRD |=(B00000001<<(intpin-14));
      PORTE &= ~(B00000001 << (intpin + 2));
    }
    if (intpin == 3) {
      PORTE &= ~(B00000001 << (intpin + 2));
    }

    if (intpin == 4) {
      //DDRD |=(B00000001<<(intpin-14));
      PORTG &= ~(B00000001 << (intpin + 1));
    }
    if (intpin == 5) {
      PORTE &= ~(B00000001 << (intpin - 2));
    }
  }
}

void forward2(int motor, int in1, int in2, int in3, int in4, int dl, int regim) {
  //  if ((motor==mProd) | (motor == mVert)| (motor==mPoper)) {

  mydigitalWrite( in1, HIGH );
  mydigitalWrite( in2, HIGH );
  mydigitalWrite( in3, HIGH );
  //mydigitalWrite( in4, LOW );
  delayMicroseconds(dl);
  mydigitalWrite( in1, HIGH );
  mydigitalWrite( in2, LOW );
  mydigitalWrite( in3, LOW );
  delayMicroseconds(dl);
}

void backward2(int motor, int in1, int in2, int in3, int in4, int dl, int regim) {
  // if ((motor==mProd) | (motor==mVert)  | (motor==mPoper)) {
  mydigitalWrite( in1, LOW );
  mydigitalWrite( in2, HIGH );
  mydigitalWrite( in3, HIGH );
  //mydigitalWrite( in4, LOW );
  delayMicroseconds(dl);
  mydigitalWrite( in1, LOW );
  mydigitalWrite( in2, LOW );
  mydigitalWrite( in3, LOW );
  delayMicroseconds(dl);
}



