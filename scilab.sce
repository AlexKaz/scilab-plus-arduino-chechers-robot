// Copyright (C) Alexander Kazantsev (AlexKaz, alexkazancev@bk.ru)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


// This is a Scilab code. It shows the board and send code to arduino board when buttons are pushed. 

// v01 05/04/2019

// If this code is usuful for you donate please, https://paypal.me/alvlk , https://yoomoney.ru/to/410019721796762

 
 clc; clear; clear all;
 
 unix("mode COM6 BAUD=9600 PARITY=n DATA=8")
 
 global regim i0 j0 game board board0 buts board_buttons
 global status_window
 global service_buttons service_buttons_names

 i0=1;
 j0=1;
 regim = %f;
 game = %f;
 buts=zeros(1,5);
 buts = [0 0 0 0 0 ] //#, x1,y1, x2,y2

 
 //исходная доска
board0=[
" ","M"," ","M"," ","M"," ","M";
"M"," ","M"," ","M"," ","M"," ";
" ","M"," ","M"," ","M"," ","M";
" "," "," "," "," "," "," "," ";
" "," "," "," "," "," "," "," ";
"m"," ","m"," ","m"," ","m"," ";
" ","m"," ","m"," ","m"," ","m";
"m"," ","m"," ","m"," ","m"," "];
//текущая доска
board=board0;
 
 
function board_buttons_push(i,j)
    global i0 j0 buts service_buttons board_buttons game
    //disp(string(i&" "&j&":"&board_buttons(i,j).string))
//    if regim then
//        S="R1111"
//        unix("echo "+S +" > COM6")
//    end
if game==%f then
    put_text_to_window("ручной режим");
    unix("mode COM6 BAUD=9600 PARITY=n DATA=8")
    unix("echo "+string("R1")+string(i0)+string(j0)+string("1") +" > COM6")
    disp(string(i)+" "+string(j)+":"+board_buttons(i,j).string)
    disp("echo X"+string(i) +" > COM6")
    unix("echo X"+string(i) +" > COM6")
    disp("echo Y"+string(j) +" > COM6")
    unix("echo Y"+string(j) +" > COM6")
    i0=i;
    j0=j;
else
    unix("mode COM6 BAUD=9600 PARITY=n DATA=8")
      disp(string(i)+" "+string(j)+":"+board_buttons(i,j).string)
    unix("echo "+string("R1")+string(i0)+string(j0)+string("1") +" > COM6")
        disp("echo X"+string(i) +" > COM6")
    unix("echo X"+string(i) +" > COM6")
        disp("echo Y"+string(j) +" > COM6")
    unix("echo Y"+string(j) +" > COM6")


    if buts(1,1)==0 then
            disp("hvat_on");
            buts(1,1)=1; buts(1,2)=i; buts(1,3)=j;
            hvat(1,i0,j0)
    elseif buts(1,1)==1 then
            disp("hvat_off");
            buts(1,4)=i; buts(1,5)=j;
            buts(1,1)=0;
            hvat(0,i0,j0)
    end
    i0=i;
    j0=j;
    unix("echo "+string("R1")+string(i0)+string(j0)+string("1") +" > COM6")
end
    //disp(j)
endfunction



okno_upravlenie=gcf(); // создать окно для управления
    okno_upravlenie.figure_size= [500,600];
    okno_upravlenie.figure_name= "Международные шашки 8х8";
    okno_upravlenie.figure_position = [63,486];
    //okno_upravlenie.pixel_drawing_mode="xor";//3 - copy, 6 - xor
    okno_upravlenie.anti_aliasing="off"; //2x,4x,8x,16x
    anti_aliasing="off"; //2x,4x,8x,16x

scf(okno_upravlenie); // Make graphic window 0 the current figure
for j=1:8
    for i=1:8
          board_buttons(i,j)=uicontrol(okno_upravlenie,...
                "style","pushbutton",...
                "units", "normalized",...
                "position", [((i-1))*1/8/1.2 0.15+((j-1))*1/8/1.2 1/8 1/8],...
                "string",board(9-j,(i)),...
                "callback"  , "board_buttons_push("+string(i)+","+string(j)+")");
    //uicontrol(f, "style", "pushbutton", "units", "normalized", "position", [0 0 0.5 0.5], "string", "Button", "horizontalalignment", "center");
    end 
end

function start_game()
    global game board board0 regim i0 j0 buts
   
    game = %t
    regim = %t
    board=board0;
    put_text_to_window("начинаем игру, ваш ход белыми");
    unix("mode COM6 BAUD=9600 PARITY=n DATA=8")
    i0=1; 
    j0=1;
    unix("echo R1"+string(i0)+string(j0)+"1 > COM6")
    disp(buts)
    buts(1,1)=0;
endfunction

function stop_game()
    global game
    game=%f
    regim = %f
endfunction

function service_buttons_pushed(i)
    global regim i0 j0 service_buttons buts
    
    select i
        case 1 then
            S="X1"
        case 2 then
            S="Xm1"
        case 3 then
            S="Y1"
        case 4 then
            S="Ym1"
        case 5 then
            S="Z1"
        case 6 then
            S="Zm1"
        case 9 then
            if regim==%T then
                S="R0"
                unix("mode COM6 BAUD=9600 PARITY=n DATA=8")
            else
                S="R1111"
                i0=1; j0=1;
                unix("mode COM6 BAUD=9600 PARITY=n DATA=8")
            end
            regim=~regim
    end
    unix("mode COM6 BAUD=9600 PARITY=n DATA=8")
    if  i~=7 & i~=8 then
        unix("echo "+S +" > COM6")
        disp ("regim"+string(regim))
    end
    if i==7 then
        start_game();
        disp("start")
        buts(1,1)=0
    end
    if i==8 then
        stop_game();
        disp("stor")
        buts(1,1)=0;
    end
endfunction

function hvat(i,m0,k0)
    
    unix("mode COM6 BAUD=9600 PARITY=n DATA=8");
    S="R0"; unix("echo "+S +" > COM6");
    
    if i==1 then
        //вниз, схватить шашку и вверх
            //S="Zm1";  unix("echo "+S +" > COM6");
             S="Zm1";  unix("echo "+S +" > COM6");
             S="Zm1";  unix("echo "+S +" > COM6");
        S="H045";  unix("echo "+S +" > COM6");
             //S="Z1";  unix("echo "+S +" > COM6");
             S="Z1";  unix("echo "+S +" > COM6");
             S="Z1";  unix("echo "+S +" > COM6");
    elseif i==0 then
        //вниз, отпустить шашку и вверх
            //S="Zm1";  unix("echo "+S +" > COM6");
            S="Zm1";  unix("echo "+S +" > COM6");
            S="Zm1";  unix("echo "+S +" > COM6");
        S="Hm045";  unix("echo "+S +" > COM6");
            //S="Z1";  unix("echo "+S +" > COM6");
            S="Z1";  unix("echo "+S +" > COM6");
            S="Z1";  unix("echo "+S +" > COM6");
    end
    unix("mode COM6 BAUD=9600 PARITY=n DATA=8")
        disp("echo R1"+string(m0)+string(k0)+"1 > COM6")
    unix("echo R1"+string(m0)+string(k0)+"1 > COM6")
endfunction

function put_text_to_window(t)
    global status_window
    status_window.String=t
endfunction

service_buttons_names=["X+", "X-", "Y+", "Y-", "Z+", "Z-","Start","Stop","Regim","Hvat_on","Hvat_off"]
for i=1:9 do
    service_buttons(i)=uicontrol(okno_upravlenie,...
                "style","pushbutton",...
                "units", "normalized",...
                "position", [1/1.1 1-(i*0.1) 1/10 1/10],...
                "string",service_buttons_names(i),...
                "callback"  ,"service_buttons_pushed("+string(i)+")");
end
service_buttons(7).position=[1/1.15 1-(7*0.1) 1/7 1/10];
service_buttons(8).position=[1/1.15 1-(8*0.1) 1/7 1/10];
service_buttons(9).position=[1/1.15 1-(9*0.1) 1/7 1/10];

i=10; //hvat_on
service_buttons(i)=uicontrol(okno_upravlenie,...
                "style","pushbutton",...
                "units", "normalized",...
                "position", [0 0  1/5 1/10],...
                "string",service_buttons_names(i),...
                "callback"  ,"hvat(1,"+string(i0)+","+string(j0)+")");
i=11;//hvat_off
service_buttons(i)=uicontrol(okno_upravlenie,...
                "style","pushbutton",...
                "units", "normalized",...
                "position", [1/5 0  1/5 1/10],...
                "string",service_buttons_names(i),...
                "callback"  ,"hvat(0,"+string(i0)+","+string(j0)+")");





status_window_text = "начало"; 

status_window=uicontrol(okno_upravlenie,...
                "Style","text",...
                "string",status_window_text,...
                "position", [200 0 300 45],...
                "margins", [5 5 5 5], ...
                "constraints", createConstraints("gridbag", [1, 1, 1, 1], [1, 1], "horizontal", "center"), ...
                "horizontalAlignment", "left");
                
                
cd "C:\Work\Arduino\!Manipulator\shashki\Draughts-master\"


//    button_stop=uicontrol(okno_upravlenie,...
//                "style","pushbutton",...
//                "units", "normalized",...
//                "position", [0.5 0 0.5 0.5],...
//                "string","$Stop$",...
//                 "callback"  , "main_loop_stop()",...
//                 "callback_type", 10);
////    button_stop=uicontrol(okno_upravlenie,...
////                  "style","radiobutton",...
////                  "position", [25,140,60,20],...
////                  "string","Остановка",...
////                  "value",1);





//// Include an editable table into a figure:
//// Building a table of data:
//params = [" " "Country" "Population [Mh]" "Temp.[Â°C]" ];
//towns = ["Mexico" "Paris" "Tokyo" "Singapour"]';
//country = ["Mexico" "France" "Japan" "Singapour"]';
//pop  = string([22.41 11.77 33.41 4.24]');
//temp = string([26 19 22 17]');
//table = [params; [ towns country pop temp ]]
//
//f = gcf();
//clf
//as = f.axes_size;  // [width height]
//ut = uicontrol("style","table",..
//               "string",table,..
//               "position",[5 as(2)-100 300 87],.. // => @top left corner of figure
//               "tooltipstring","Data from majors towns")
//
//// Modify by hand some values in the table. Then get them back from the ui:
//matrix(ut.string,size(table))


